using UnityEditor;
using UnityEngine;

namespace Editor
{
    [CustomEditor(typeof(ColorController))]
    public class ColorSetter : UnityEditor.Editor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            ColorController colorController = (ColorController) target;

            if (GUILayout.Button("Set Color"))
            {
                colorController.SetColor();
            }

            if (GUI.changed)
            {
                EditorUtility.SetDirty(target);
            }
        }
    }
}
