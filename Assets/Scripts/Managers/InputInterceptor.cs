using System;
using UnityEngine;
using UnityEngine.EventSystems;

public class InputInterceptor : MonoBehaviour, IPointerUpHandler, IPointerDownHandler, IDragHandler
{
    public static Action<Vector2> OnDown;
    public static Action<Vector2> OnUp;
    public static Action<Vector2> OnDragEvent;
    
    
    
    public void OnPointerUp(PointerEventData eventData) =>
        OnUp?.Invoke(eventData.position);

    public void OnPointerDown(PointerEventData eventData)
    {
        OnDown?.Invoke(eventData.position);
    }

    public void OnDrag(PointerEventData eventData) => 
        OnDragEvent?.Invoke(eventData.position);
}
