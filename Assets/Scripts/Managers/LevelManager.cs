using System;
using Gameplay;
using UnityEngine;

namespace Managers
{
    public class LevelManager : MonoBehaviour
    {
    
        #region Fields

        [SerializeField] private Transform playerSpawnPoint;
        [SerializeField] private Transform cameraSpawnPoint;
        [SerializeField] private Transform cameraRoot;
    
        private GameObject playerPrefab;
        private GameObject levelCamera;
        private GameObject cameraPrefab;
        private GameObject player;

        private DeathController playerDeathController;

        #endregion

        public static event Action<GameObject> OnPlayerSpawn;


    
        #region Class lifecycle

        //TODO:Have to encapsulate resource loading in resource manager or something like this.
        private void Awake()
        {
            playerPrefab = Resources.Load<GameObject>("Prefabs/Gameplay/Player/Ela");
            cameraPrefab = Resources.Load<GameObject>("Prefabs/Other/MainCamera");
            
            player = Instantiate(playerPrefab, playerSpawnPoint);
            player.transform.SetParent(transform);

            levelCamera = Instantiate(cameraPrefab, cameraRoot);
            levelCamera.transform.position = cameraSpawnPoint.position;
            levelCamera.GetComponent<CameraFollow>().Target = player.transform.GetChild(0).transform;
            
            playerDeathController = player.GetComponent<DeathController>();
        }

        private void Start() => OnPlayerSpawn?.Invoke(player);

        private void OnEnable() => playerDeathController.OnPlayerDied += DeathController_OnPlayerDied;

        private void OnDisable() => playerDeathController.OnPlayerDied -= DeathController_OnPlayerDied;

        #endregion


        #region Private Methods

        #endregion

        #region Event Handlers

        private void DeathController_OnPlayerDied()
        {
            player.transform.position = playerSpawnPoint.transform.position;
        }

        #endregion
    
    }
}
