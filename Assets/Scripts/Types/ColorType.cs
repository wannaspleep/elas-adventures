namespace Types
{
    public enum ColorType
    {
        Purple,
        Orange,
        LightGreen,
        Yellow,
        Cyan,
        Pink
    }
}