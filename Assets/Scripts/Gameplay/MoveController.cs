using UnityEngine;
using Vector2 = UnityEngine.Vector2;

namespace Gameplay
{
    public class MoveController : MonoBehaviour
    {
        
        #region Fields

        [SerializeField] [Range(1, 20)] private int jumpPower;


        private CollisionController collisionController;
        private TriggerController triggerController;
        
        private Rigidbody2D playerRigidbody;
        private Vector2 startTapPosition;
        private Vector2 endTapPosition;

        private TrajectoryDrawer trajectoryDrawer;
        private Coroutine trajectoryUpdateCoroutine;
        private bool isGrounded;

        #endregion

        
        
        #region Class lifecycle

        private void Awake()
        {
            playerRigidbody = GetComponent<Rigidbody2D>();
            collisionController = GetComponent<CollisionController>();
            triggerController = GetComponent<TriggerController>();
            
            trajectoryDrawer = new TrajectoryDrawer();
            trajectoryDrawer.Initialize();
        }


        private void OnEnable()
        {
            InputInterceptor.OnDown += InputInterceptor_OnDown;
            InputInterceptor.OnUp += InputInterceptor_OnUp;
            InputInterceptor.OnDragEvent += InputInterceptor_OnDragEvent;
            
            collisionController.OnEnter += CollisionController_OnEnter;
            collisionController.OnExit += CollisionController_OnExit;
            collisionController.OnStay += CollisionController_OnStay;

            triggerController.OnEnter += TriggerController_OnEnter;
            triggerController.OnExit += TriggerController_OnExit;
            triggerController.OnStay += TriggerController_OnStay;
        }


        private void OnDisable()
        {
            InputInterceptor.OnDown -= InputInterceptor_OnDown;
            InputInterceptor.OnUp -= InputInterceptor_OnUp;
            InputInterceptor.OnDragEvent -= InputInterceptor_OnDragEvent;
            
            collisionController.OnEnter -= CollisionController_OnEnter;
            collisionController.OnStay -= CollisionController_OnStay;
            collisionController.OnExit -= CollisionController_OnExit;
            
            triggerController.OnEnter -= TriggerController_OnEnter;
            triggerController.OnExit -= TriggerController_OnExit;
            triggerController.OnStay -= TriggerController_OnStay;
        }


        private void Update()
        {
            if (trajectoryDrawer.IsActive)
            {
                Vector2 directionVector = startTapPosition - endTapPosition;
                Vector2 clampedDirection = Vector2.ClampMagnitude(directionVector, 1f);
                trajectoryDrawer.UpdateTrajectory(transform.position, clampedDirection, jumpPower);
            }
        }

        #endregion

        
        
        #region Event Handlers

        private void InputInterceptor_OnDragEvent(Vector2 position)
        {
            endTapPosition = position;
        }

        private void InputInterceptor_OnUp(Vector2 position)
        {
            if (isGrounded)
            {
                Vector2 direction = startTapPosition - endTapPosition;
                Vector2 normalizedDirection = direction.normalized;
            
                playerRigidbody.velocity = Vector2.zero;
                playerRigidbody.AddForce(normalizedDirection * jumpPower, ForceMode2D.Impulse);
            }
            
            trajectoryDrawer.EnableTrajectory(false);
        }

        private void InputInterceptor_OnDown(Vector2 position)
        {
            startTapPosition = position;
            trajectoryDrawer.EnableTrajectory(true);
        }
        
        
        private void CollisionController_OnEnter(Collision2D collision2D)
        {
            if (collision2D.gameObject.CompareTag("Slide"))
            {
                isGrounded = true;
            }
            
            foreach (ContactPoint2D contact in collision2D.contacts)
            {
                Debug.DrawRay(contact.point, contact.normal * 10, Color.red);
            }
        }
        
        
        private void CollisionController_OnStay(Collision2D other)
        {
            if(other.gameObject.CompareTag("Slide"))
            {
                isGrounded = true;
            }
        }

        
        private void CollisionController_OnExit(Collision2D collision2D)
        {
            if (collision2D.gameObject.CompareTag("Slide"))
            {
                //isGrounded = false;
            }
        }


        private void TriggerController_OnExit(Collider2D other)
        {
            if (other.gameObject.CompareTag("Water"))
            {
                isGrounded = false;
            }
        }


        private void TriggerController_OnEnter(Collider2D other)
        {
            if (other.gameObject.CompareTag("Water"))
            {
                isGrounded = true;
            }
        }
        
        
        private void TriggerController_OnStay(Collider2D other)
        {
            if (other.gameObject.CompareTag("Water"))
            {
                isGrounded = true;
            }
        }

        #endregion

    }
}
