using System;
using UnityEngine;

public class TriggerController : MonoBehaviour
{
    public event Action<Collider2D> OnEnter;
    public event Action<Collider2D> OnStay;
    public event Action<Collider2D> OnExit;


    private void OnTriggerEnter2D(Collider2D other) =>
        OnEnter?.Invoke(other);

    private void OnTriggerExit2D(Collider2D other) => 
        OnExit?.Invoke(other);

    private void OnTriggerStay2D(Collider2D other) =>
        OnStay?.Invoke(other);
}
