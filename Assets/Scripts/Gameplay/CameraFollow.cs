using System;
using Managers;
using UnityEngine;

namespace Gameplay
{
    public class CameraFollow : MonoBehaviour
    {
        private Transform target;

        [SerializeField] private float smoothSpeed = 0.125f;
        [SerializeField] private Vector3 offset;
        

        private bool isPlayerSpawned;

        public Transform Target
        {
            get => target;
            set => target = value;
        }

        private void OnEnable()
        {
            LevelManager.OnPlayerSpawn += LevelManager_OnPlayerSpawn;
        }

        private void OnDisable()
        {
            LevelManager.OnPlayerSpawn -= LevelManager_OnPlayerSpawn;
        }

        void LateUpdate()
        {

            Vector3 desiredPosition = target.localPosition + offset;
            Vector3 smoothedPosition = Vector3.Lerp(transform.position, desiredPosition, smoothSpeed);
            Vector3 cameraCurrentPosition = new Vector3(transform.position.x, smoothedPosition.y, transform.position.z);
            transform.position = cameraCurrentPosition;

        }

        private void LevelManager_OnPlayerSpawn(GameObject player)
        {
            target = player.transform;
            isPlayerSpawned = true;
        }
    }
}
