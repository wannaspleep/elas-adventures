using System;
using UnityEngine;

namespace Gameplay
{
    public class CollisionController : MonoBehaviour
    {
        public event Action<Collision2D> OnEnter;
        public event Action<Collision2D> OnStay;
        public event Action<Collision2D> OnExit;

        private void OnCollisionEnter2D(Collision2D collision2D) => 
            OnEnter?.Invoke(collision2D);

        private void OnCollisionStay2D(Collision2D other) => 
            OnStay?.Invoke(other);

        private void OnCollisionExit2D(Collision2D other) => 
            OnExit?.Invoke(other);
    }
}