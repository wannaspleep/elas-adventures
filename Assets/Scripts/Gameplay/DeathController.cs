using System;
using Gameplay;
using UnityEngine;

public class DeathController : MonoBehaviour
{
    public event Action OnPlayerDied;
    
    private CollisionController collisionController;
    private ColorController colorController;


    private void Awake()
    {
        collisionController = GetComponent<CollisionController>();
        colorController = GetComponent<ColorController>();
    }

    private void OnEnable()
    {
        collisionController.OnEnter += CollisionController_OnEnter;
    }

    private void OnDisable()
    {
        collisionController.OnEnter -= CollisionController_OnEnter;
    }

    private void CollisionController_OnEnter(Collision2D other)
    {
        if (other.gameObject.TryGetComponent(out ColorController otherColorController))
        {
            if (colorController.GetColor() != otherColorController.GetColor())
            {
                OnPlayerDied?.Invoke();
            }
        }
    }
}
