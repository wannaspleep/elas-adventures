using Types;
using UnityEngine;
using UnityEngine.U2D;
using UnityEditor;

public class ColorController : MonoBehaviour
{
    [SerializeField] private Transform bodyTransform;

    [SerializeField] private ColorType colorType = ColorType.Cyan;
    private SpriteRenderer spriteRenderer;
    private SpriteShapeRenderer spriteShapeRenderer;

    

    #region Properties

    public ColorType GetColor() => colorType;

    #endregion
    
    
    
    #region Class lifecycle


    private void Start()
    {
        //SetColor();
    }

    private void OnEnable()
    {
    }

    private void OnDisable()
    {
    }

    #endregion


    public void SetColor()
    {
        Color objectColor = GetColorByEnum(colorType);

        Debug.Log($"{objectColor}");
        if (bodyTransform.TryGetComponent(out spriteRenderer))
        {
            spriteRenderer.color = objectColor;
        }
        else if (bodyTransform.TryGetComponent(out spriteShapeRenderer))
        {
            spriteShapeRenderer.color = objectColor;
        }
        else
        {
            Debug.LogError("There aren't attached spriteRenderer or SpriteShapeRenderer to the object");
        }

    }

    private Color GetColorByEnum(ColorType type)
    {
        switch (colorType)
        {
            case ColorType.Purple:
                return new Color(115, 93, 249, 1);
            case ColorType.Orange:
                return new Color(250, 124, 78, 1);
            case ColorType.LightGreen:
                return new Color(162, 250, 50, 1);
            case ColorType.Yellow:
                return new Color(250, 204, 1, 1);
            case ColorType.Cyan:
                return new Color(75, 209, 250, 1);
            case ColorType.Pink:
                return new Color(245, 60, 250, 1);
        }
        return Color.white;
    }
}
