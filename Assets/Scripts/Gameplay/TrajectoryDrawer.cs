using System.Numerics;
using UnityEngine;
using Vector2 = UnityEngine.Vector2;
using Vector3 = UnityEngine.Vector3;

namespace Gameplay
{
   public class TrajectoryDrawer
   {
      private GameObject[] dots;
      private GameObject dotPrefab;
      private int minDotsCount;
      private int maxDotsCount = 10;
      private float spaceBetweenPoints = 0.1f;
      
      public bool IsActive { get; set; }



      public void Initialize()
      {
         dotPrefab = Resources.Load<GameObject>("Prefabs/Gameplay/Trajectory/DotPrefab");
         dots = new GameObject[maxDotsCount];
         
         for (int i = 0; i < maxDotsCount; i++)
         {
            dots[i] = Object.Instantiate(dotPrefab);
            
            GameObject currentDot = dots[i];
            currentDot.transform.position = new Vector3(0, i, 1);
            currentDot.GetComponentInChildren<SpriteRenderer>().color = new Color(1,1,1,1 - i * 0.1f);
            currentDot.SetActive(false);
         }
      }


      public void UpdateTrajectory(Vector2 startPosition, Vector2 direction, float strength)
      {
         for (int i = 0; i < dots.Length; i++)
         {
            dots[i].transform.position = 
               CalculatePointPosition(startPosition, direction, strength, spaceBetweenPoints * i);
            
         }
      }


      public void EnableTrajectory(bool isEnable)
      {
         foreach (GameObject dot in dots)
         {
            dot.SetActive(isEnable);
         }

         IsActive = isEnable;
      }


      private Vector2 CalculatePointPosition
         (Vector2 startPosition, Vector2 direction, float force, float t) =>
         startPosition + new Vector2(direction.x * force * t, direction.y * force * t) +
         0.5f * Physics2D.gravity * t * t;
   }
}
