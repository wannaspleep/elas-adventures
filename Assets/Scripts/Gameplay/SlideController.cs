using System;
using System.Collections;
using DG.Tweening;
using UnityEngine;
using Debug = UnityEngine.Debug;

namespace Gameplay
{
    public class SlideController : MonoBehaviour
    {
        #region Fields

        [SerializeField] private float surfaceDelay;

        private GameObject lastCollidedSlideObject;
        private CollisionController collisionController;
        private TriggerController triggerController;
        private ColorController colorController;

        private bool isSameColor;
        private Tween temporaryDisableEffector;

        #endregion

        
        
        #region Unity lifecycle

        private void Awake()
        {
            collisionController = GetComponent<CollisionController>();
            triggerController = GetComponent<TriggerController>();
            colorController = GetComponent<ColorController>();
        }


        private void OnEnable()
        {
            InputInterceptor.OnUp += InputInterceptor_OnUp;
            
            collisionController.OnEnter += CollisionController_OnEnter;
            collisionController.OnExit += CollisionController_OnExit;

        }


        private void OnDisable()
        {
            InputInterceptor.OnUp -= InputInterceptor_OnUp;
            
            collisionController.OnEnter -= CollisionController_OnEnter;
            collisionController.OnExit -= CollisionController_OnExit;

            //delayedCall?.Kill();
        }

        #endregion

        
        
        #region Event Handlers

        private void CollisionController_OnExit(Collision2D collision)
        {
            isSameColor = false;
        }

        
        private void CollisionController_OnEnter(Collision2D collision)
        {
            if (collision.gameObject.TryGetComponent(out ColorController otherColorController))
            {
                if (colorController.GetColor() == otherColorController.GetColor())
                {
                    isSameColor = true;
                }
            }
        
            if (collision.gameObject.CompareTag("Slide") && isSameColor)
            {
                lastCollidedSlideObject = collision.gameObject;
            }
        }

        
        private void InputInterceptor_OnUp(Vector2 _)
        {
            if (lastCollidedSlideObject != null)
            {
                if (lastCollidedSlideObject.CompareTag("Slide") && lastCollidedSlideObject.TryGetComponent(out SurfaceEffector2D effector))
                {
                    TemporaryDisableEffector(effector);
                }
            }
        }

        private void TemporaryDisableEffector(SurfaceEffector2D effector)
        {
            effector.enabled = false;
            Debug.Log($"Effector disabled for {effector.gameObject.name}");

            temporaryDisableEffector?.Complete();

            temporaryDisableEffector = DOVirtual.DelayedCall(surfaceDelay, () =>
                {
                    effector.enabled = true;
                    Debug.Log($"Effector enabled for {effector.gameObject.name}");
                })
                .OnComplete(() => Debug.Log("I'm complete"))
                .OnKill(() => Debug.Log("Somebody killed me"));
        }

        #endregion


        
        #region Coroutines

       
        #endregion
    }
    
}
